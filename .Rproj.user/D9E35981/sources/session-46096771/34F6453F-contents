#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)
library(DT)

Date<-"04-15-2024"


Final <- readRDS(file.path(getwd(), 'Data/Final_REF.rds'))
# Define UI for application that draws a histogram
ui <- fluidPage(
  h1(id="big-heading", "RI REF Dashboard"),
  tags$style(HTML("#big-heading{background-color: White; color: navy; }")),
  h5 (id = "QA-email", "For any questions, please email QAInspections.RI@cadmusgroup.com"),
  tags$style(HTML("#QA-email{color: steelblue; }")),
  h6 (id = "small-heading-1","Updates are scheduled for Fridays at 7:30PM EST" ),
  tags$style(HTML("#small-heading-1{color: steelblue; }")),
  h6 (id = "small-heading-2", paste("Last update:",Date)),
  tags$style(HTML("#small-heading-2{color: steelblue; }")),
  h1 (id = "space", " "),
  h1 (id = "space2", " "),
  h1 (id = "space3", " "),
  h6 (id = "space6", " "),
  tags$style(HTML("#space{background-color: Green;}")),

  # Create a new Row in the UI for selectInputs
  fluidRow(
    column(6,
           selectInput("Installer",
                       "Installer:",
                       c("All",
                         unique(as.character(Final$Installer ))))
    ),
    
    column(6,
           selectInput("Action Required by",
                       "Action Required by:",
                       c("All",
                         unique(as.character(Final$`Action Required by:`))))
    ),
    
    column(6,
           selectInput("Issues",
                       "Identified Issues:",
                       c("All", 
                         as.character( c("5 - No Issues", "4 - Incidental", "3 - Minor", "2 - Major",  "1 - Critical", "N/A" ))
                       ))
    ), 
    
    column(6,
           selectInput("Status",
                       "Inspection Status:",
                       c("All", 
                         unique(as.character( Final$`Inspection Status` ))
                       ))
    )
    
    
    
    
    
  ),
  # Create a new row for the table.
  DT::dataTableOutput("table" )
)

# Define server logic required to draw a histogram
server <- function(input, output) {
  
  # Filter data based on selections
  output$table <- DT::renderDT(filter = 'top', options = list(
    pageLength = 15,
    initComplete = JS(
      "function(settings, json) {",
      "$(this.api().table().header()).css({'background-color': 'steelblue', 'color': 'white'});",
      "}")),
      
    
    
    {
        data <- Final
        if (input$`Installer` != "All") {
          data <- data[data$`Installer` == input$`Installer`,]
        }
        if (input$`Action Required by` != "All") {
          data <- data[data$`Action Required by:` == input$`Action Required by`,]
        }
        if (input$`Issues` != "All" ) {
          data <- data[data$`Identified Issues` == input$`Issues`,]
        }
        if (input$`Status` != "All" ) {
          data <- data[data$`Inspection Status` == input$`Status`,]
        }
        data
      })
 
}

# Run the application 
shinyApp(ui = ui, server = server)
